import express from 'express'
import morgan from 'morgan';
import cors from 'cors';
import userRoutes from "./routes/user.router";
import eventRoutes from "./routes/event.router"
import ticketRoutes from "./routes/ticketReserva.router"
import bookRoutes from "./routes/bookingTicket.router"

import passportMiddleware from './middlewares/passport';
import passport from 'passport'
import passportLocal from "passport-local";
//conector de Swagger
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import { options } from './swaggerOptions';

const app = express()


app.use(morgan('dev'));
app.use(cors());
app.use(express.json());

app.use("/api", userRoutes);
app.use("/api", eventRoutes);
app.use("/api", ticketRoutes)
app.use("/api", bookRoutes)

//Agregar para jwt
app.use(express.urlencoded({ extended: false }));
app.use(passport.initialize());
passport.use(passportMiddleware);

//Agrego Swagger
const spect = swaggerJSDoc(options);
app.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(spect)
)

app.use("/api/users", userRoutes)
app.use("/api/events", eventRoutes)

export default app;