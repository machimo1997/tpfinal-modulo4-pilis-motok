import { Request, Response } from "express";
import { TicketRes } from "../entity/TicketReserva";
import { Events } from "../entity/Events";
import { BookingTicket } from "../entity/BookingTicket";
//import limite from "../middlewares/middleware";
//import limite from "../middlewares/middleware"






export const getTickets = async (req: Request, res: Response) => {
    console.log('entrando...');
    try {
        const ticket = await TicketRes.find();

        console.log('ticket: --->'), ticket;
        return res.json(ticket);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};


export const getTicket = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;

        const ticket = await TicketRes.findOneBy({ id: parseInt(id) });

        if (!ticket) return res.status(404).json({ message: "Event not found" });

        return res.json(ticket);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};


export const createTicket = async (req: Request, res: Response) => {
    console.log("entrando al post de creacion evento")
    try {
        const { usuario, evento } = req.body;

        const ticket = new TicketRes();
        ticket.evento = evento
        ticket.usuario = usuario

        const ticketValidado = await Events.findOneBy({ id: parseInt(evento) });
        if (ticketValidado?.limite != 0) {
            await ticket.save();
            const cambio = Number(ticketValidado?.limite) - 1
            Events.update({ id: evento }, { limite: cambio })
        } else {
            return res.status(101).json({ message: `Ya no queda espacios para este evento` })
        }


        const booking = new BookingTicket();
        booking.evento = evento
        booking.usuario = usuario
        booking.ticketReserva = ticket

        await booking.save();

        return res.json(ticket);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }

};


export const updateTicket = async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
        const event = await TicketRes.findOneBy({ id: parseInt(id) });

        if (!event) return res.status(404).json({ message: "Not event found" });

        await TicketRes.update({ id: parseInt(id) }, req.body);

        return res.sendStatus(204);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};


export const deleteTicket = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await TicketRes.delete({ id: parseInt(id) });

        if (result.affected === 0)
            return res.status(404).json({ message: "Event not found" });

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};