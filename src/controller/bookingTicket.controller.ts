import { Request, Response } from "express";
import { BookingTicket } from "../entity/BookingTicket";


export const getBookings = async (req: Request, res: Response) => {
    console.log('entrando...');
    try {
        const ticket = await BookingTicket.find();

        console.log('ticket: --->'), ticket;
        return res.json(ticket);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};


export const getBook = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;

        const ticket = await BookingTicket.findOneBy({ id: parseInt(id) });

        if (!ticket) return res.status(404).json({ message: "Event not found" });

        return res.json(ticket);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

export const deleteBook = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await BookingTicket.delete({ id: parseInt(id) });

        if (result.affected === 0)
            return res.status(404).json({ message: "Event not found" });

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};