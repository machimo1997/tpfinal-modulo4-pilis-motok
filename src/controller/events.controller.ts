import { Request, Response } from "express";
import { Events } from "../entity/Events";



export const getEventos = async (req: Request, res: Response) => {
    console.log('entrando...');
    try {

        const users = await Events.find();

        console.log('users: --->'), users;
        return res.json(users);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};


export const getEvent = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;

        const user = await Events.findOneBy({ id: parseInt(id) });

        if (!user) return res.status(404).json({ message: "Event not found" });

        return res.json(user);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};


export const createEvent = async (req: Request, res: Response) => {
    console.log("entrando al post de creacion evento")
    try {
        const { nombre, tipoEvento, precio, limite, descripcion, fechaHora, lugar, gps } = req.body;

        //Date.parse(fechaHora)
        const date = new Date(fechaHora)

        const event = new Events();
        event.nombre = nombre;
        event.descripcion = descripcion;
        event.lugar = lugar;
        event.fechaHora = date;
        event.gps = gps;
        event.precio = precio;
        event.limite = limite;
        event.tipoEvento = tipoEvento;

        await event.save();

        return res.json(event);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }

};


export const updateEvent = async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
        const event = await Events.findOneBy({ id: parseInt(id) });

        if (!event) return res.status(404).json({ message: "Not event found" });

        await Events.update({ id: parseInt(id) }, req.body);

        return res.sendStatus(204);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};


export const deleteEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await Events.delete({ id: parseInt(id) });

        if (result.affected === 0)
            return res.status(404).json({ message: "Event not found" });

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};