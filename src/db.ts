import { DataSource } from "typeorm";
import { User } from "./entity/User";
import { Events } from "./entity/Events";
import { BookingTicket } from "./entity/BookingTicket";
import { TicketRes } from "./entity/TicketReserva";

export const AppDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "mysql",
    database: "ticket-system-TPFinal-Tickets",
    // logging: true, // muestra peticiones a la bd
    synchronize: true,
    entities: [User, Events, BookingTicket, TicketRes],
});