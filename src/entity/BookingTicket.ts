import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, OneToMany, OneToOne, JoinColumn, ManyToOne } from "typeorm";
import { User } from "./User"
import { Events } from "./Events";
import { TicketRes } from "./TicketReserva";

@Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'
export class BookingTicket extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;


    //@OneToMany(() => User, (user) => user.email)
    // usuario: User;
    @ManyToOne(() => TicketRes)
    ticketReserva: TicketRes

    @ManyToOne(() => User)
    usuario: User

    @ManyToOne(() => Events)
    evento: Events


    @JoinColumn()
    reserva: TicketRes


    @OneToOne(() => Events, (event) => event.precio)
    precioEvento: Number


    @OneToOne(() => Events, (event) => event.fechaHora)
    fechaHoraEvento: Date


    @OneToOne(() => Events, (event) => event.lugar)
    lugarEvento: string


    @OneToOne(() => Events, (event) => event.gps)
    gpsEvento: string

}