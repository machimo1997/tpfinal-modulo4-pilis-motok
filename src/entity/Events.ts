import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { TicketRes } from "./TicketReserva";

@Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'
export class Events extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    descripcion: string;

    @Column()
    lugar: string;

    @Column()
    fechaHora: Date;

    @Column()
    gps: string;

    @Column()
    precio: Number;

    @Column()
    limite: Number;

    @Column()
    tipoEvento: string;

    @OneToMany(() => TicketRes, (ticket) => ticket.evento)
    reserva: TicketRes[]
}