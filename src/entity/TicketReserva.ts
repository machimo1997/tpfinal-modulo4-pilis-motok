import { Entity, PrimaryGeneratedColumn, ManyToOne, BaseEntity, JoinColumn, CreateDateColumn } from "typeorm"
import { User } from "./User"
import { Events } from "./Events"

@Entity()
export class TicketRes extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number


    @ManyToOne(() => User, (usuario) => usuario.reserva)
    usuario: User


    @ManyToOne(() => Events, (events) => events.reserva)
    evento: Events

    @CreateDateColumn()
    createdAt: Date;
}
