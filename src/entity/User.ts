import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { TicketRes } from "./TicketReserva";

@Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    email: string;

    @Column({ nullable: false })
    password: string;

    @Column({ default: true })
    active: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(() => TicketRes, (ticket) => ticket.usuario)
    reserva: TicketRes[]
}