import { Router } from "express";
import { getBookings, getBook, deleteBook } from "../controller/bookingTicket.controller";

const router = Router();

router.get("/book", getBookings);

router.get("/book/:id", getBook);

router.delete("/book/:id", deleteBook);

export default router;