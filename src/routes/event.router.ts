import { Router } from "express";
import { getEvent, getEventos, updateEvent, createEvent, deleteEvent } from "../controller/events.controller";

const router = Router();

router.get("/event", getEventos);

router.get("/event/:id", getEvent);
router.post("/event", createEvent);
router.put("/event/:id", updateEvent);
router.delete("/event/:id", deleteEvent);

export default router;