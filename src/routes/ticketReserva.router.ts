import { Router } from "express";
import { getTicket, getTickets, createTicket, updateTicket, deleteTicket } from "../controller/TicketReserva.controller";

const router = Router();
router.get("/ticket", getTicket)
router.get("/tickets", getTickets)
router.post("/ticket", createTicket)
router.put("/ticket/:id", updateTicket);
router.delete("/ticket/:id", deleteTicket);


export default router;