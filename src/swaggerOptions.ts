export const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "API",
            version: "1.0.0",
            description: "Una simple API de la libreria express"
        },
        servers: [
            {
                url: "http://localhost:3000",
            },
        ],
    },

    apis: [`${__dirname}/docs/**/*.yaml`],

}